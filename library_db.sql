CREATE DATABASE IF NOT EXISTS `library_db`
 /*!40100 DEFAULT CHARACTER SET utf8mb4 */ 
 /*!80016 DEFAULT ENCRYPTION='N' */;
USE `library_db`;

CREATE TABLE IF NOT EXISTS `author` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255)
);

CREATE TABLE IF NOT EXISTS `book` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `author_id` int,
  `title` varchar(255)
);

ALTER TABLE `book` ADD FOREIGN KEY (`author_id`) REFERENCES `author` (`id`);

-- Add mock data here 
insert into author (id, name) values (1, 'Author 1');
insert into book (id, author_id, title) values (1, 1, 'Book 1.1');
insert into book (id, author_id, title) values (2, 1, 'Book 1.2');
insert into author (id, name) values (2, 'Author 2');
insert into book (id, author_id, title) values (3, 2, 'Book 2.1');
insert into author (id, name) values (3, 'Author 3');
insert into book (id, author_id, title) values (4, 3, 'Book 3.1');
insert into book (id, author_id, title) values (5, 3, 'Book 3.2');
insert into book (id, author_id, title) values (6, 3, 'Book 3.3');
insert into book (id, author_id, title) values (7, 3, 'Book 3.4');


