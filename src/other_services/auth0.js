const request = require('request')
require('dotenv').config()

// returns a management API access token for Auth0
export async function getAuth0AccessToken() {
   let options = {
      method: 'POST',
      url: `https://${process.env.AUTH0_DOMAIN}/oauth/token`,
      headers: { 'content-type': 'application/x-www-form-urlencoded' },
      form: {
         grant_type: 'client_credentials',
         client_id: process.env.AUTH0_MANAGEMENT_CLIENT_ID,
         client_secret: process.env.AUTH0_MANAGEMENT_CLIENT_SECRET,
         audience: `https://${process.env.AUTH0_DOMAIN}/api/v2/`,
      },
   }

   return new Promise((resolve, reject) => {
      request(options, function (error, response, body) {
         if (error) throw new Error(error)
         let token = JSON.parse(body).access_token
         resolve(token)
      })
   })
}

// returns a list of user roles attached to the user
export function getUserRoles(userID, accessToken) {
   let options = {
      method: 'GET',
      url: `https://${process.env.AUTH0_DOMAIN}/api/v2/users/${userID}/roles`,
      headers: { authorization: `Bearer ${accessToken}` },
   }

   return new Promise((resolve, reject) => {
      request(options, function (error, response, body) {
         if (error) throw new Error(error)
         let roles = JSON.parse(body)
         roles = roles.map((role) => role.name)
         resolve(roles)
      })
   })
}

// Returns a list of all users with the given name
export async function getUsersByName(name, accessToken) {
   var options = {
      method: 'GET',
      url: `https://${process.env.AUTH0_DOMAIN}/api/v2/users`,
      qs: { q: `name:${name}*`, search_engine: 'v3' },
      headers: { authorization: `Bearer ${accessToken}` },
   }

   return new Promise((resolve, reject) => {
      request(options, function (error, response, body) {
         if (error) throw new Error(error)
         resolve(body)
      })
   })
}

// add more auth0 functions here..
