const Sequelize = require('sequelize')
const path = require('path')
const fs = require('fs')

// export  a reference to the database
module.exports = createDBRef()

// Create the db object
function createDBRef() {
   var db = {}
   const sequelize = createSQLConnection()
   const models = getAllModels()
   db = assignModels(db, models, sequelize)
   db = applyAssociations(db)
   db = addSequelizeInstanceToDb(db, sequelize)
   return db
}

// creates a connection to the SQL database
function createSQLConnection() {
   return new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASSWD, {
      host: process.env.DB_IP,
      port: process.env.DB_PORT,
      dialect: 'mysql',
      define: {
         freezeTableName: true,
      },
      pool: {
         max: 5,
         min: 0,
         acquire: 30000,
         idle: 10000,
      },
      logging: false,
      operatorsAliases: false,
      insecureAuth: true,
   })
}

// gets all models in the db_models subdirectory
function getAllModels() {
   var pathToModels = path.join(__dirname, '../src/database/models')
   var files = fs.readdirSync(pathToModels)
   var models = []
   files.forEach((file) => {
      models.push(require('src/database/models/' + file))
   })
   return models
}

// assigns each model to the db object
function assignModels(db, models, sequelize) {
   models.forEach((model) => {
      const seqModel = model(sequelize, Sequelize)
      db[seqModel.name] = seqModel
   })
   return db
}

// associate each model with the database
function applyAssociations(db) {
   Object.keys(db).forEach((key) => {
      if ('associate' in db[key]) {
         db[key].associate(db)
      }
   })
   return db
}

// add references to the sequelize connection and package to the db object
function addSequelizeInstanceToDb(db, sequelize) {
   db.sequelize = sequelize
   db.Sequelize = Sequelize
   return db
}
