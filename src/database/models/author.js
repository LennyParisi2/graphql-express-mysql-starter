/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('author', {
		id: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		name: {
			type: DataTypes.STRING(255),
			allowNull: true
		}
	}, {
		tableName: 'author',
		timestamps: false
	});
};
