/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('book', {
		id: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		author_id: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'author',
				key: 'id'
			}
		},
		title: {
			type: DataTypes.STRING(255),
			allowNull: true
		}
	}, {
		tableName: 'book',
		timestamps: false
	});
};
