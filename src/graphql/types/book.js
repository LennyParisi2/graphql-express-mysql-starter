import { gql } from 'apollo-server-express'

export const typeDefs = gql`
   type Book {
      id: ID!
      author_id: ID!
      title: String!
   }
`
