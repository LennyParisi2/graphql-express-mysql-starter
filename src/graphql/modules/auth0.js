import { gql } from 'apollo-server-express'
import { getAuth0AccessToken, getUserRoles, getUsersByName } from '../../other_services/auth0'

export const typeDefs = gql`
   extend type Query {
      get_current_user_roles: [String]
      get_users_by_name(name: String!): String
   }
`

export const resolvers = {
   Query: {
      get_user_roles: async (obj, args, context) => {
         if (context.user != undefined) {
            // TODO: instead of requesting a token every call, we need to store it
            // and only get a new one when the token has expired
            let accessToken = await getAuth0AccessToken()
            return getUserRoles(context.user.sub, accessToken)
         }
         return null
      },
      get_users_by_name: async (obj, args) => {
         if (context.user != undefined) {
            // TODO: instead of requesting a token every call, we need to store it
            // and only get a new one when the token has expired
            let accessToken = await getAuth0AccessToken()
            return getUsersByName(args.name, accessToken)
         }
         // This would happen if you were in 'dev' mode and you tried calling
         // an auth0 function without supplying an auth0 access token
         return null
      },
   },
}
