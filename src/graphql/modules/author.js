import * as db from 'src/database/database'
import { gql } from 'apollo-server-express'

export const typeDefs = gql`
   extend type Author {
      books_written: [Book]
   }

   extend type Query {
      get_all_authors: [Author]
      get_author_by_id(id: ID!): Author
      get_author_by_name(name: String!): Author
   }

   extend type Mutation {
      insert_author(id: ID!, name: String!): Author
   }
`

export const resolvers = {
   Author: {
      books_written: async (obj) =>
         db.book.findAll({
            where: {
               author_id: obj.id,
            },
         }),
   },
   Query: {
      get_all_authors: async () => db.author.findAll(),
      get_author_by_id: async (obj, args) => db.author.findByPk(args.id),
      get_author_by_name: async (obj, args) =>
         db.author.findOne({
            where: {
               name: {
                  [db.Sequelize.Op.like]: args.name + '%',
               },
            },
         }),
   },
   Mutation: {
      insert_author: async (obj, args) => db.author.create(args),
   },
}
