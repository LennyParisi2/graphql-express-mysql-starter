import * as db from 'src/database/database'
import { gql } from 'apollo-server-express'

export const typeDefs = gql`
   extend type Book {
      author: Author
   }

   extend type Mutation {
      insert_book(id: ID!, author_id: ID!, title: String!, date_published: String!): Book
   }
`

export const resolvers = {
   Book: {
      author: async (obj) =>
         db.author.findOne({
            where: {
               id: obj.author_id,
            },
         }),
   },
   Mutation: {
      insert_book: async (obj, args) => db.book.create(args),
   },
}
