import '@babel/polyfill'
require('dotenv').config()
const { ApolloServer } = require('apollo-server-express')
const cors = require('cors')
const path = require('path')
const fs = require('fs')
const jwt = require('express-jwt')
const jwksRsa = require('jwks-rsa')
const express = require('express')

// A server object that is used to initialize the express app,
// setup routes, start the apollo server, and handle authentication
class Server {
   // Initialize fields
   constructor() {
      // Reference to the current directory (used to load files from other directories)
      this.currentDirectory = path.join(__dirname, '../')
      this.expressApp = this.initializeApp()
      this.apolloServer = this.initializeApolloServer()
   }

   // Initializes the express app
   initializeApp() {
      let expressApp = express()
      expressApp.use(express.static('./doc/schema'))
      expressApp.use(express.json({ limit: '50mb' }))
      expressApp.use(express.urlencoded({ limit: '50mb' }))
      expressApp.use(cors())
      if (!process.argv.includes('--dev'))
         expressApp.use(this.processAccessToken(process.env.AUTH0_DOMAIN, process.env.AUTH0_AUDIENCE))
      return expressApp
   }

   // Middleware that ensures the access token is valid
   processAccessToken(issuer, audience) {
      return jwt({
         secret: jwksRsa.expressJwtSecret({
            cache: true,
            rateLimit: true,
            jwksRequestsPerMinute: 5,
            jwksUri: `https://${issuer}/.well-known/jwks.json`,
         }),

         audience,
         issuer: `https://${issuer}/`,
         algorithms: ['RS256'],
      })
   }

   // initializes the apollo server and reads:
   // - the types defined in the src/graphql/types directory to create the GraphQL types
   // - the queries and mutations defined in the /src/graphql/modules directory to create the GraphQL
   //     queries, mutations, and resolvers
   initializeApolloServer() {
      var modules = this.getGraphqlTypes().concat(this.getGraphQLModules())
      return new ApolloServer({
         // Pass in the modules here
         modules,
         context: ({ req }) => ({
            // Pass in a reference to the user linked to the auth0 access token
            user: req.user,
         }),
      })
   }

   // returns all graphql types in the src/graphql/types directory
   getGraphqlTypes() {
      var types = []
      var typeNames = this.getFilesInDir('src/graphql/types')
      typeNames.forEach((type) => {
         types.push(require('src/graphql/types/' + type))
      })
      return types
   }

   // returns all graphql modules in the src/graphql/modules directory
   getGraphQLModules() {
      var modelNames = this.getFilesInDir('src/graphql/modules')
      var graphqlModels = []
      modelNames.forEach((model) => {
         graphqlModels.push(require('src/graphql/modules/' + model))
      })
      return graphqlModels
   }

   // gets all files in a given directory
   getFilesInDir(dir) {
      var pathToGraphqlModels = path.join(this.currentDirectory, dir)
      var files = fs.readdirSync(pathToGraphqlModels)
      return files
   }

   // starts the app and runs on given port
   start() {
      var app = this.expressApp
      var port = process.env.PORT
      this.apolloServer.applyMiddleware({ app })

      // Make '/' redirect to /graphql endpoint
      this.addRoute('/', function (req, res) {
         res.redirect('/graphql')
      })

      this.expressApp.listen({ port: port }, () => console.log(`Server ready. Listening on port ` + port))
   }

   // adds a route to the express app
   addRoute(route, callback) {
      this.expressApp.get(route, callback)
   }
}

export default Server
