# Getting started

To get started, you need to install dependencies.

    npm install
Next,  you need to create a MySQL database. The project includes a .sql file, but if you want to quickly copy and paste the SQL to create it, here it is: 

    -- Create the database
    CREATE  DATABASE  IF  NOT  EXISTS  `library_db`
    
    /*!40100 DEFAULT CHARACTER SET utf8mb4 */
    
    /*!80016 DEFAULT ENCRYPTION='N' */;
    
    USE  `library_db`;   
    
    -- Create the tables
    
    CREATE  TABLE  IF  NOT  EXISTS  `author` (
    
    `id`  int  PRIMARY  KEY AUTO_INCREMENT,
    
    `name`  varchar(255)
    
    );
    
    CREATE  TABLE  IF  NOT  EXISTS  `book` (
    
    `id`  int  PRIMARY  KEY AUTO_INCREMENT,
    
    `author_id`  int,
    
    `title`  varchar(255)
    
    );
    
    -- Create the constraint
    
    ALTER  TABLE  `book` ADD FOREIGN KEY (`author_id`) REFERENCES  `author` (`id`);
    
    -- Add mock data here
    
    insert  into author (id, name) values (1, 'Author 1');
    
    insert  into book (id, author_id, title) values (1, 1, 'Book 1.1');
    
    insert  into book (id, author_id, title) values (2, 1, 'Book 1.2');
    
    insert  into author (id, name) values (2, 'Author 2');
    
    insert  into book (id, author_id, title) values (3, 2, 'Book 2.1');
    
    insert  into author (id, name) values (3, 'Author 3');
    
    insert  into book (id, author_id, title) values (4, 3, 'Book 3.1');
    
    insert  into book (id, author_id, title) values (5, 3, 'Book 3.2');
    
    insert  into book (id, author_id, title) values (6, 3, 'Book 3.3');
    
    insert  into book (id, author_id, title) values (7, 3, 'Book 3.4');

# Configuration 
To be continued...
# Authentication
To be continued
# Custom database integration
### 1. Generating models
If you have a custom database that you want to use for this project, the first thing you need to is generate models for them. To do this, go to the package.json file and enter all of the information needed in the ""generate-models" script. You can also just run this command one: 

    sequelize-auto -h DATABASE_HOST -d DATABASE_NAME -u DATABASE_USER -x DATABASE_USER -p DATABASE_PORT --dialect mysql -c ./sequalize_auto_settings.json -o ./src/database/models/

This will connect to your database, generate the models, and output them to src/database/models. 
### 2. Creating the GraphQL types*
You need to create (or generate) the GraphQL types that correspond to each model in your database and put them in the **src/graphql/types directory**. Do not include any additional queries, mutations, or resolvers in these files. These files are merely to define the base types for each model. 
### 3. Creating the GraphQL modules*
Next, you'll want to create the GraphQL modules that correspond to each model if you wish to add queries, mutations, additional fields, or resolvers to a GraphQL type. 
### 4. (Optional) Add additional routes to your Express app
If you wish to add additional routes to your express app, simply open the **index.js** file and type the following before the `server.start()` call.

    server.addRoute("/ROUTE",function(req,res)=>{
		// Add stuff here
	})

###### *Refer to the existing files in the project for reference. 