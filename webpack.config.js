var path = require('path')
var nodeExternals = require('webpack-node-externals')

module.exports = {
   node: {
      fs: 'empty',
      net: 'empty',
      __dirname: false,
   },
   target: 'node',
   externals: [nodeExternals()],
   mode: 'development',
   devtool: 'inline-source-map',
   entry: path.resolve(__dirname, 'src', 'index.js'),
   output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'index.js',
   },
   resolve: {
      // Add `.ts` and `.tsx` as a resolvable extension.
      extensions: ['.graphql', '.jsx', '.js'],
      modules: [path.resolve(__dirname), path.resolve(__dirname, './src')],
   },
   module: {
      rules: [
         // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
         {
            test: /\.jsx?$/,
            loader: 'babel-loader',
            exclude: [/node_modules/],
            options: {
               presets: ['@babel/preset-env'],
            },
         },
      ],
   },
}
